import { environment } from './../../environments/environment';

export default {
  oidc: {
    clientId: '0oa1l968w2FFhHe1r5d7',
    issuer: 'https://dev-58617279.okta.com/oauth2/default',
    redirectUri: 'http://localhost:4200/login/callback',
    scopes: ['email', 'profile','openid']
  }
}
