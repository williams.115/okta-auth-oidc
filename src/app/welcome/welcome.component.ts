import { Component, OnInit } from '@angular/core';
import { OktaAuthStateService } from '@okta/okta-angular';
import { OktaAuth } from '@okta/okta-auth-js';
import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {

  userName = '';
  token: any ;
  tdecode: any ;
  userClaims: any ;

  constructor(
    public authStateService: OktaAuthStateService,
    private oktaAuth : OktaAuth
  ) { }

  async ngOnInit() {

    const isAuthenticated = await this.oktaAuth.isAuthenticated();

    if (isAuthenticated) {

      this.userClaims = await this.oktaAuth.getUser();
      this.token =  this.oktaAuth.getAccessToken();
      this.tdecode = jwt_decode(this.token);
      this.userName = this.userClaims.name as string;
    }
  }
}
